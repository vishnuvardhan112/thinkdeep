package JavaConcepts.Variables;

public class RunFast {


    public static void main(String[] args) {

        Rati  rt = new Rati();
        Anusha anu = new Anusha();
        Siva  anything = new Siva();
        Vanitha  vani = new Vanitha();
        Venkatesh venky = new Venkatesh();

        System.out.println("-----------"+rt.preferedName+"--------------");
        System.out.println(rt.preferedName+" favourite color is "+rt.favouriteColour);
        System.out.println(rt.preferedName+" birth place is "+rt.birthPlace);
        System.out.println(rt.preferedName+" lucky number is "+rt.luckyNumber);
        System.out.println(rt.preferedName+" first travel outside the India is "+rt.yearOfAbroadTrip);
        System.out.println(rt.preferedName+" NRI status is "+rt.nriStatus);

        System.out.println("-----------"+anu.preferedname+"--------------");
        System.out.println(anu.preferedname+" favourite color is "+anu.favouriteColour);
        System.out.println(anu.preferedname+" birth place is "+anu.birthPlace);
        System.out.println(anu.preferedname+" lucky number is "+anu.luckyNumber);
        System.out.println(anu.preferedname+" first travel outside the India is "+anu.yearOfAbroadTrip);
        System.out.println(anu.preferedname+" NRI status is "+anu.nriStatus);

        System.out.println("-----------"+anything.preferedName+"--------------");
        System.out.println(anything.preferedName+" favourite color is "+anything.favouriteColour);
        System.out.println(anything.preferedName+" birth place is "+anything.birthPlace);
        System.out.println(anything.preferedName+" lucky number is "+anything.luckyNumber);
        System.out.println(anything.preferedName+" first travel outside the India is "+anything.yearOfAbroadTrip);
        System.out.println(anything.preferedName+" NRI status is "+anything.nriStatus);

        System.out.println("-----------"+vani.preferedName+"--------------");
        System.out.println(vani.preferedName+" favourite color is "+vani.favouriteColour);
        System.out.println(vani.preferedName+" birth place is "+vani.birthPlace);
        System.out.println(vani.preferedName+" lucky number is "+vani.luckyNumber);
        System.out.println(vani.preferedName+" first travel outside the India is "+vani.yearOfAbroadTrip);
        System.out.println(vani.preferedName+" NRI status is "+vani.nriStatus);

        System.out.println("-----------"+venky.preferedName+"--------------");
        System.out.println(venky.preferedName+" favourite color is "+venky.favouriteColour);
        System.out.println(venky.preferedName+" birth place is "+venky.birthPlace);
        System.out.println(venky.preferedName+" lucky number is "+venky.luckyNumber);
        System.out.println(venky.preferedName+" first travel outside the India is "+venky.yearOfAbroadTrip);
        System.out.println(venky.preferedName+" NRI status is "+venky.nriStatus);

    }


}
